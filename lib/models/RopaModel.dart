class RopaModel {

  int _id;
  int _idTienda;
  int _idTipo;
  String _descripcion;
  double _precio;
  List<String> _urlImages;
  int _contador;


  RopaModel(this._id, this._idTienda, this._idTipo,
      this._descripcion, this._precio, this._urlImages, this._contador);

  factory RopaModel.fromJson(Map<String, dynamic> json){
    return RopaModel(
      json['ID_ROPA'],
      json['ID_SHOP'],
      json['ID_TIP'],
      json['DES_ROP'],
      json['PRE_ROPA'],
      json['FOTO_ROPA'],
      json['CANT_ROPA'],
    );
  }

  int get idTienda => _idTienda;

  set idTienda(int value) {
    _idTienda = value;
  }

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  int get contador => _contador;

  set contador(int value) {
    _contador = value;
  }

  List<String> get urlImages => _urlImages;

  set urlImages(List<String> value) {
    _urlImages = value;
  }

  double get precio => _precio;

  set precio(double value) {
    _precio = value;
  }

  String get descripcion => _descripcion;

  set descripcion(String value) {
    _descripcion = value;
  }

  int get idTipo => _idTipo;

  set idTipo(int value) {
    _idTipo = value;
  }

}

final listaRopaModelo = <RopaModel>[

  RopaModel(1, 1, 1, 'Blusa blabla', 12.00, ['https://www.shasa.com/media/catalog/product/cache/1/small_image/409x572/9df78eab33525d08d6e5fb8d27136e95/1/7/1702831102002_1_1.jpg'], 0),
  RopaModel(2, 1, 3, 'Zapatos blabla', 24.00, ['https://www.sufavorita.com/22974/zapato-plataforma-mujer-durazno-negro-marca-top-moda.jpg'], 0),
  RopaModel(3, 2, 2, 'Pantalones blabla', 15.00, ['https://images-na.ssl-images-amazon.com/images/I/51Y4IzFzKyL._UX679_.jpg'], 0),
  RopaModel(4, 2, 1, 'Blusa blabla', 10.00, ['https://i.pinimg.com/originals/b5/77/bf/b577bf46057c577dad88a5a85db4b398.jpg','https://www.petuniaropa.com/wp-content/uploads/2018/09/1Blusa-Pepas-Blanca.jpg'], 0),
  RopaModel(5, 3, 4, 'Lenceria blabla', 18.00, ['https://http2.mlstatic.com/baby-doll-lenceria-D_NQ_NP_17927-MLA20146796745_082014-O.jpg'], 0),
  RopaModel(6, 3, 4, 'Lenceria 2 blabla', 22.00, ['https://i.ebayimg.com/images/g/F6cAAOSwzaJX-giN/s-l300.jpg'], 0),

];

final listaTipoRopaModelo = <Map<String, dynamic>>[
  {
    'id' : 0,
    'descripcion' : 'Todo',
  },
  {
    'id' : 1,
    'descripcion' : 'Blusas',
  },
  {
    'id' : 2,
    'descripcion' : 'Pantalones',
  },
  {
    'id' : 3,
    'descripcion' : 'Zapatos',
  },
  {
    'id' : 4,
    'descripcion' : 'Lenceria',
  }
];

// 1 Blusas
// 2 Pantalones
// 3 Zapatos
// 4 Lenceria