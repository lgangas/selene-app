class TiendaModel {
  int _id;
  String _nombre;
  String _descripcion;
  String _fuente;
  String _urlImagen;
  String _colorPrimario;
  String _colorSecundario;

  TiendaModel(this._id, this._nombre, this._descripcion, this._fuente,
      this._urlImagen, this._colorPrimario, this._colorSecundario);

  String get colorSecundario => _colorSecundario;

  set colorSecundario(String value) {
    _colorSecundario = value;
  }

  String get colorPrimario => _colorPrimario;

  set colorPrimario(String value) {
    _colorPrimario = value;
  }

  String get urlImagen => _urlImagen;

  set urlImagen(String value) {
    _urlImagen = value;
  }

  String get fuente => _fuente;

  set fuente(String value) {
    _fuente = value;
  }

  String get descripcion => _descripcion;

  set descripcion(String value) {
    _descripcion = value;
  }

  String get nombre => _nombre;

  set nombre(String value) {
    _nombre = value;
  }

  int get id => _id;

  set id(int value) {
    _id = value;
  }
}

final listaTienda = <TiendaModel>[
  TiendaModel(1, 'Bliss', 'Venta de ropa para mujeres', 'Pacifico', 'https://www.blooming-lotus-yoga.com/bliss/wp-content/uploads/sites/2/2016/06/BLISS-Magazine-by-Blooming-Lotus-Yoga-1-440x264.jpg', '', ''),
  TiendaModel(2, 'Sick Girls', 'Venta de ropa para mujeres', 'Pacifico', 'https://4.bp.blogspot.com/-IOViwaHeetE/WHAe3F5JCwI/AAAAAAAAYsQ/QNkmI2jf5go0k-Hg5OFyHDDATHtZdOvrACLcB/s1600/11215166_1034428756581943_22890761839594757_n.jpg', '', ''),
  TiendaModel(3, 'Las Venecas', 'Venta de ropa para mujeres', 'Pacifico', 'http://www.tutiendavecina.com/fotografiastiendas/tienda_ropa_mujer_en_logrono_coco.jpg', '', ''),
];