import 'package:flutter/material.dart';
import 'package:seline_app/screens/busquedaRopaTienda.dart';
import 'package:seline_app/screens/tienda.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {
  
  TabController _tabController;
  
  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Material(
        child: TabBarView(
          controller: _tabController,
          children: [
            BusquedaRopaTienda(),
            Tienda(tiendaModel: null,)
          ]
        ),
      ),
      bottomNavigationBar: Container(
        color: Color(0xff4D5B68),
        child: TabBar(
          indicator: UnderlineTabIndicator(
            borderSide: BorderSide(color: Color(0xff7C90A4), width: 3.0),
            insets: EdgeInsets.only(bottom: 45.0),
          ),
          controller: _tabController,
          tabs: [
            Tab(icon: Icon(FontAwesomeIcons.tshirt),),
            Tab(icon: Icon(FontAwesomeIcons.store),),
          ]
        ),
      ),
    );
  }
}
