import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:seline_app/models/RopaModel.dart';
import 'package:seline_app/models/TiendaModel.dart';
import 'package:seline_app/screens/detalleRopa.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:seline_app/screens/tienda.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../utils/ConstantsUrl.dart';

class BusquedaRopaTienda extends StatefulWidget {
  @override
  _BusquedaRopaTiendaState createState() => _BusquedaRopaTiendaState();
}

class _BusquedaRopaTiendaState extends State<BusquedaRopaTienda> with SingleTickerProviderStateMixin {
  bool _busqueda = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Color(0xffe6f2ff)
//          gradient: LinearGradient(colors: [Color(0xff499BEA),Color(0xff71B1EE)],begin: Alignment.topCenter, end: Alignment.bottomCenter),
        ),
        child: _busqueda ? ScreenBusquedaRopa() : ScreenBusquedaTienda(),
      ),
    );
  }

  Widget appBar(){
    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: Color(0xff4D5B68),
      title: Text(
        'Seline',
        style: TextStyle(
          fontFamily: 'Pacifico',
          color: Colors.white,
        ),
      ),
      actions: <Widget>[
        switchTiendaRopa()
      ],
    );
  }

  Widget switchTiendaRopa(){
    return Padding(
      padding: EdgeInsets.only(right: 16.0),
      child: Row(
        children: <Widget>[
          RaisedButton(
            color: Colors.greenAccent,
            onPressed: _busqueda ? null : (){
              setState(() {
                _busqueda = !_busqueda;
              });
            },
            shape: OutlineInputBorder(
              borderSide: BorderSide.none,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0),bottomLeft: Radius.circular(20.0))
            ),
            child: Icon(FontAwesomeIcons.tshirt,color: Colors.white,),
          ),
          RaisedButton(
            color: Colors.greenAccent,
            shape: OutlineInputBorder(
              borderSide: BorderSide.none,
              borderRadius: BorderRadius.only(topRight: Radius.circular(20.0),bottomRight: Radius.circular(20.0))
            ),
            onPressed: _busqueda ? (){
              setState(() {
                _busqueda = !_busqueda;
              });
            } : null,
            child: Icon(FontAwesomeIcons.store,color: Colors.white,),
          ),
        ],
      ),
    );
  }
}

class ScreenBusquedaTienda extends StatefulWidget {
  @override
  _ScreenBusquedaTiendaState createState() => _ScreenBusquedaTiendaState();
}

class _ScreenBusquedaTiendaState extends State<ScreenBusquedaTienda> {

  List<TiendaModel> _listaTienda = listaTienda;

  @override
  void initState() {
    super.initState();
  }

  void _getListaTienda(String text){
    setState(() {
      _listaTienda = listaTienda.where((tienda)=>tienda.nombre.toUpperCase().contains(text.toUpperCase())).toList();
    });
  }

  void _onTapTienda(tienda){
    Navigator.push(context, MaterialPageRoute(builder: (context)=> Tienda(tiendaModel: tienda)));
  }


  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        busquedaTiendaInput(),
        listaTiendaView()
      ],
    );
  }

  Widget listaTiendaView(){
    return SliverPadding(
      padding: EdgeInsets.all(8.0),
      sliver: SliverStaggeredGrid.countBuilder(
        staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
        crossAxisCount: 2,
        itemCount: _listaTienda.length,
        itemBuilder: (context,index){
          return InkWell(
            onTap: ()=> _onTapTienda(_listaTienda[index]),
            child: Card(
              elevation: 5.0,
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(3.0),
//                      child: Hero(
//                        tag: 'tag-first-image-${_listaRopa[index].id}',
                    child: Image.network(_listaTienda[index].urlImagen,fit: BoxFit.fill,width: double.infinity,)
//                      ),
                  ),
                  Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(3.0),bottomRight: Radius.circular(3.0)),
                        gradient: LinearGradient(
                          colors: [Colors.transparent,Colors.black54],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter
                        )
                      ),
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            _listaTienda[index].nombre,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 20.0,
                              fontFamily: 'Montserrat',
                              color: Colors.white
                            )
                          ),
//                          Text(
//                              '\$ ${_listaRopa[index].precio.toString()}',
//                              style: TextStyle(
//                                  fontSize: 22.0,
//                                  fontFamily: 'Montserrat',
//                                  color: Colors.white
//                              )
//                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget busquedaTiendaInput(){
    return SliverPadding(
      padding: EdgeInsets.all(8.0),
      sliver: SliverToBoxAdapter(
          child: SizedBox(
            height: 50.0,
            child: TextField(
              onChanged: _getListaTienda,
              style: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.white
              ),
              decoration: InputDecoration(
                prefixIcon: Icon(FontAwesomeIcons.search,color: Colors.white,),
                fillColor: Color(0xff283037),
                filled: true,
                hintText: 'Buscar tienda',
                hintStyle: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Colors.white
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(20.0)
                )
              ),
            ),
          ),
      ),
    );
  }

}


class ScreenBusquedaRopa extends StatefulWidget {
  @override
  _ScreenBusquedaRopaState createState() => _ScreenBusquedaRopaState();
}

class _ScreenBusquedaRopaState extends State<ScreenBusquedaRopa> with SingleTickerProviderStateMixin {

  int _indiceTipoRopa = 0;
  List<RopaModel> _listaRopa = [];
  TabController _tabControllerTipoRopa;

  @override
  void initState() {
    super.initState();
    _tabControllerTipoRopa = TabController(length: listaTipoRopaModelo.length, vsync: this);
    getListaRopa();
  }

  @override
  void dispose() {
    _tabControllerTipoRopa.dispose();
    super.dispose();
  }

  Future<void> getListaRopa()async{
//    var response = await http.get('${ConstantsUrl().API_SERVICIOS}/api/listadoRopa');
//
//    if(response.statusCode == 200){
//      List<Map<String, dynamic>> jsonArray = json.decode(response.body);
//      List<RopaModel> newListaRopa = jsonArray.map((ropa){
//        return RopaModel.fromJson(ropa);
//      });
//      setState(() {
//        if(_indiceTipoRopa == 0){
//          _listaRopa = newListaRopa;
//        } else {
//          _listaRopa = newListaRopa.where((ropa)=>ropa.idTipo == _indiceTipoRopa).toList();
//        }
//      });
//    }
    setState(() {
      if(_indiceTipoRopa == 0){
        _listaRopa = listaRopaModelo;
      } else {
        _listaRopa = listaRopaModelo.where((ropa)=>ropa.idTipo == _indiceTipoRopa).toList();
      }
    });
  }

  void _onTapRopa(ropa){
    Navigator.of(context).push(MaterialPageRoute(builder: (context)=>DetalleRopa(ropaModel: ropa,)));
  }


  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        listaTipoRopaView(),
        listaRopaView()
      ],
    );
  }

  Widget listaRopaView(){
    return SliverPadding(
      padding: EdgeInsets.all(8.0),
      sliver: SliverStaggeredGrid.countBuilder(
        staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
        crossAxisCount: 2,
        itemCount: _listaRopa.length,
        itemBuilder:
            (context,index){
          return InkWell(
            onTap: ()=> _onTapRopa(_listaRopa[index]),
            child: Card(
              elevation: 5.0,
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                      borderRadius: BorderRadius.circular(3.0),
//                      child: Hero(
//                        tag: 'tag-first-image-${_listaRopa[index].id}',
                      child: Image.network(_listaRopa[index].urlImages[0],fit: BoxFit.fill,width: double.infinity,)
//                      ),
                  ),
                  Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(3.0),bottomRight: Radius.circular(3.0)),
                        gradient: LinearGradient(
                          colors: [Colors.transparent,Colors.black54],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter
                        )
                      ),
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                              _listaRopa[index].descripcion,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 20.0,
                                  fontFamily: 'Montserrat',
                                  color: Colors.white
                              )
                          ),
                          Text(
                              '\$ ${_listaRopa[index].precio.toString()}',
                              style: TextStyle(
                                  fontSize: 22.0,
                                  fontFamily: 'Montserrat',
                                  color: Colors.white
                              )
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget listaTipoRopaView(){
    int indiceTipoRopa = 0;
    return SliverToBoxAdapter(
        child: SizedBox(
          height: 56.0,
          child: TabBar(
              indicatorPadding: EdgeInsets.symmetric(horizontal: 16.0),
              labelColor: Color(0xffB1CFEB),
              unselectedLabelColor: Color(0xff283037),
              indicatorColor: Color(0xffB1CFEB),
              indicatorWeight: 4.0,
              isScrollable: true,
              controller: _tabControllerTipoRopa,
              onTap: (indice){
                _indiceTipoRopa = listaTipoRopaModelo[indice]['id'];
                getListaRopa();
              },
              tabs: listaTipoRopaModelo.map((tipo){
                return Tab(
                    child:Text(
                        listaTipoRopaModelo[indiceTipoRopa++]['descripcion'],
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 25.0,
                          fontFamily: 'Montserrat',
                        )
                    )
                );
              }).toList()
          ),
        )
    );
  }
}


