import 'package:flutter/material.dart';

class CrearTienda extends StatefulWidget {
  @override
  _CrearTiendaState createState() => _CrearTiendaState();
}

class _CrearTiendaState extends State<CrearTienda> {

  String _nombreTienda ='Nombre de la tienda',
         _descripcion = 'Descripcion',
         _fuente = 'Fuente',
         _colorPrimario = 'Color primario';

  Future<void> getDialogText(input, fieldName) async {
    String value = '';
    var response = await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return SimpleDialog(
          shape: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent),
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30.0),bottomRight: Radius.circular(30.0))
          ),
          title: Text(fieldName),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: TextField(
                onChanged: (text) {
                  value = text;
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FlatButton(
                    child: Text('Aceptar'),
                    onPressed: () {
                      if(value.trim() != ''){
                        Navigator.pop(context, 'accept');
                      }
                    },
                    textColor: Colors.blueAccent,
                  ),
                  FlatButton(
                    child: Text('Cancelar'),
                    onPressed: () {Navigator.pop(context);},
                    textColor: Colors.blueAccent,
                  ),
                ],
              ),
            )
          ],
        );
      }
    );

    switch(response){
      case 'accept':
        switch(input){
          case 'nombre':
            _nombreTienda = value;
            break;
          case 'descripcion':
            _descripcion = value;
            break;
          case 'fuente':
            _fuente = value;
            break;
          case 'colorPrimario':
            _colorPrimario = value;
            break;
          default:
        }
        setState(() {});
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        sliverAppBar(),
        sliverList(),
      ]
    );
  }

  Widget sliverList(){
    return SliverList(
      delegate: SliverChildListDelegate([
        myListTile(_nombreTienda, 'Nombre de la tienda', 'nombre'),
        Divider(),
        myListTile(_descripcion, 'Descripcion','descripcion'),
        Divider(),
        myListTile(_fuente, 'Fuente','fuente'),
        Divider(),
        myListTile(_colorPrimario, 'Color primario','colorPrimario'),
        Divider(),
      ]),
    );
  }

  Widget myListTile (text, fieldName, input) {
    return ListTile(
      onTap: () {getDialogText(input, fieldName);},
      title: Text(
        text,
        style: TextStyle(
          color: Color(0xff4D5B68),
          fontSize: 18.0,
        ),
      ),
    );
  }

  Widget sliverAppBar() {
    return SliverAppBar(
      flexibleSpace: FlexibleSpaceBar(
        title: Text('Mi tienda'),
        centerTitle: false,
        titlePadding: EdgeInsets.symmetric(horizontal: 10.0),
      ),
      automaticallyImplyLeading: false,
      expandedHeight: 150.0,
      floating: false,
      pinned: true,
      snap: false,
    );
  }
}
