import 'package:flutter/material.dart';
import 'package:seline_app/screens/homePage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../utils/ConstantsUrl.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  String user = '';
  String _email = '';
  String _password = '';
  String _confirmPassword = '';
  bool _isLogin = true;

  BuildContext _context ;

  final keyForm = GlobalKey<FormState>();
  final keyFormRegister = GlobalKey<FormState>();

  Future<String> getUserFromSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString('user');
  }

  @override
  void initState() {
    getUserFromSharedPreferences()
      .then((us){
        setState(() {
          user = us;
        });
    });
  }

  Future<void> _iniciarSesion()async{
    if(keyForm.currentState.validate()){
//      var response = await http.post('${ConstantsUrl().API_SERVICIOS}/api/login',body: {
//        "EMAIL_USU" : _email,
//        "PASS_USU": _password
//      });
//
//      if(response.statusCode == 200) {
//        int isValid = int.tryParse(response.body) ?? 0;
//        if(isValid == 1){
          final prefs = await SharedPreferences.getInstance();
          prefs.setString('user', _email);
          Navigator.of(context).push(MaterialPageRoute(builder: (context)=> HomePage()));
//        } else {
//          Scaffold.of(_context).showSnackBar(SnackBar(content: Text('Email o contraseña incorrectos')));
//        }
//      }
    }
  }

  Future<void> _registrarse () async {
    if(keyFormRegister.currentState.validate()){
      if(_confirmPassword.compareTo(_password) == 0){
//        var response = await http.post('${ConstantsUrl().API_SERVICIOS}/api/login',body: {
//          "EMAIL_USU" : _email,
//          "PASS_USU": _password
//        });
//
//        if(response.statusCode == 200){
          setState(() {
            _isLogin = true;
          });
//        } else {
//          Scaffold.of(_context).showSnackBar(SnackBar(content: Text('Algo salio mal')));
//        }
      } else {
        Scaffold.of(_context).showSnackBar(SnackBar(content: Text('Contraseñas deben coincidir')));
      }
    }
  }

  @override
  Widget build(BuildContext context) {


//    if(user != null){
//      return HomePage();
//    }

    return Scaffold(
      body: Builder(builder: (buildContext){
        _context = buildContext;
        return SingleChildScrollView(
          child: Center(
            child: Container(
              height: MediaQuery.of(context).size.height,
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              width: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(colors: [Color(0xff499BEA),Color(0xff71B1EE)],begin: Alignment.topCenter, end: Alignment.bottomCenter),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: _isLogin ? login(): register(),
              )
            ),
          ),
        );
      })
    );
  }

  List<Widget> register() {
    return [
      inputsRegister(),
      SizedBox(height: 10.0,),
      buttonsRegister(),
      SizedBox(height: 30.0,),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            '¿Ya tienes cuenta?',
            style: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.white,
                fontWeight: FontWeight.w400
            ),
          ),
          SizedBox(width: 5.0,),
          InkWell(
            onTap: (){
              setState(() {
                _isLogin = true;
              });
            },
            child: Text(
              'Ingresa',
              style: TextStyle(
                decoration: TextDecoration.underline,
                fontFamily: 'Montserrat',
                color: Color(0xff068BFF),
              ),
            ),
          )
        ],
      )
    ];
  }

  List<Widget> login() {
    return [
      Text(
        'Seline',
        style: TextStyle(
            fontFamily: 'Pacifico',
            color: Colors.white,
            fontSize: 52.0
        ),
      ),
      SizedBox(height: 10.0,),
      inputsLogin(),
      SizedBox(height: 10.0,),
      buttonsLogin(),
      SizedBox(height: 30.0,),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            '¿Aún no tienes cuenta?',
            style: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.white,
                fontWeight: FontWeight.w400
            ),
          ),
          SizedBox(width: 5.0,),
          InkWell(
            onTap: (){
              setState(() {
                _isLogin = false;
              });
            },
            child: Text(
              'Regístrate',
              style: TextStyle(
                decoration: TextDecoration.underline,
                fontFamily: 'Montserrat',
                color: Color(0xff068BFF),
              ),
            ),
          )
        ],
      )
    ];
  }

  Widget buttonsRegister() {
    return RaisedButton(
        color: Colors.green,
        child: Text(
          'Regìstrate',
          style: TextStyle(
            fontFamily: 'Montserrat',
            color: Colors.white,
          ),
        ),
        onPressed: () {_registrarse();}
    );
  }

  Widget buttonsLogin(){
    return Row(
      children: <Widget>[
        RaisedButton(
          color: Colors.redAccent,
          child: Text(
            'G',
            style: TextStyle(
              fontFamily: 'Montserrat',
              color: Colors.white,
              fontWeight: FontWeight.w700
            ),
          ),
          onPressed: (){}
        ),
        SizedBox(width: 5.0,),
        Expanded(
          child: RaisedButton(
            color: Colors.green,
            child: Text(
              'Iniciar sesión',
              style: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.white,
              ),
            ),
            onPressed: () {_iniciarSesion();}
          ),
        ),
        SizedBox(width: 5.0,),
        RaisedButton(
          color: Colors.blueAccent,
          child: Text(
            'F',
            style: TextStyle(
              fontFamily: 'Montserrat',
              color: Colors.white,
              fontWeight: FontWeight.w700
            ),
          ),
          onPressed: (){}
        )
      ],
    );
  }

  Widget inputsRegister() {
    return Form(
      key: keyFormRegister,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          TextFormField(
            onSaved: (text) {
              _email = text;
            },
            validator: (text){
              if(text.isEmpty){
                return 'Ingrese email';
              }
              return null;
            },
            decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(15.0),
                ),
                filled: true,
                hintText: 'Correo',
                prefixIcon: Icon(Icons.email, color: Colors.white,),
                hintStyle: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Colors.white
                ),
                fillColor: Color(0xff4D5B68)
            ),
            style: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.white
            ),
          ),
          SizedBox(height: 10.0,),
          TextFormField(
            onSaved: (text) {
              _password = text;
            },
            validator: (text){
              if(text.isEmpty){
                return 'Ingrese contraseña';
              }
              return null;
            },
            obscureText: true,
            decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(15.0),
                ),
                filled: true,
                hintText: 'Contraseña',
                prefixIcon: Icon(Icons.vpn_key, color: Colors.white,),
                hintStyle: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Colors.white
                ),
                fillColor: Color(0xff4D5B68)
            ),
            style: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.white
            ),
          ),
          SizedBox(height: 10.0,),
          TextFormField(
            onSaved: (text) {
              _confirmPassword = text;
            },
            validator: (text){
              if(text.isEmpty){
                return 'Ingrese confirmar contraseña';
              }
              return null;
            },
            obscureText: true,
            decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(15.0),
                ),
                filled: true,
                hintText: 'Confirmar contraseña',
                prefixIcon: Icon(Icons.vpn_key, color: Colors.white,),
                hintStyle: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Colors.white
                ),
                fillColor: Color(0xff4D5B68)
            ),
            style: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.white
            ),
          ),
        ],
      ),
    );
  }

  Widget inputsLogin(){
    return Form(
      key: keyForm,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          TextFormField(
            onSaved: (text) {
              _email = text;
            },
            validator: (text){
              if(text.isEmpty){
                return 'Ingrese email';
              }
              return null;
            },
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(15.0),
              ),
              filled: true,
              hintText: 'Correo',
              prefixIcon: Icon(Icons.email, color: Colors.white,),
              hintStyle: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.white
              ),
              fillColor: Color(0xff4D5B68)
            ),
            style: TextStyle(
              fontFamily: 'Montserrat',
              color: Colors.white
            ),
          ),
          SizedBox(height: 10.0,),
          TextFormField(
            onSaved: (text) {
              _password = text;
            },
            validator: (text){
              if(text.isEmpty){
                return 'Ingrese contraseña';
              }
              return null;
            },
            obscureText: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(15.0),
              ),
              filled: true,
              hintText: 'Contraseña',
              prefixIcon: Icon(Icons.vpn_key, color: Colors.white,),
              hintStyle: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.white
              ),
              fillColor: Color(0xff4D5B68)
            ),
            style: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.white
            ),
          ),
        ],
      ),
    );
  }
}
