import 'package:flutter/material.dart';
import 'package:seline_app/models/RopaModel.dart';
import 'package:seline_app/models/TiendaModel.dart';

class DetalleRopa extends StatefulWidget {

  final RopaModel ropaModel;

  DetalleRopa({this.ropaModel});


  @override
  _DetalleRopaState createState() => _DetalleRopaState();
}

class _DetalleRopaState extends State<DetalleRopa> {

  double _height;
  double _width;
  List _listaImagenes;
  List<Widget> _indicadoresImagen;


  PageController _pageController;

  @override
  void initState() {
    super.initState();
//    _pageController = PageController()
    _listaImagenes = widget.ropaModel.urlImages;
    _crearIndicadoresImagen(0);
  }

  void _crearIndicadoresImagen(int indicador){
    int contador = 0;
    setState(() {
      _indicadoresImagen = _listaImagenes.map((imagen){
        return Expanded(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 2.0),
            height: 3.0,
            color: indicador == contador++ ? Colors.white : Colors.black54,
          ),
        );
      }).toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          contenedorImagenes(),
          contenedorDetalle()
        ],
      ),
    );
  }

  Widget contenedorImagenes(){
    return Positioned(
      top: 0,
      child: Container(
        width: _width,
        height: _height / 2.0,
        child: Stack(
          children: <Widget>[
            PageView.builder(
                onPageChanged: (indice)=> _crearIndicadoresImagen(indice),
                itemCount: _listaImagenes.length,
                itemBuilder: (context, indice){
                  return Image.network(_listaImagenes[indice],fit: BoxFit.fill,);
                }
            ),
          ],
        ),
      ),
    );
  }

  Widget contenedorDetalle(){
    return Positioned(
      top: _height / 2 - 20.0,
      child: Container(
        height: _height * 2 / 3,
        width: _width,
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: [Color(0xff499BEA),Color(0xff71B1EE)],begin: Alignment.topCenter, end: Alignment.bottomCenter),
            color: Colors.red,
            borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))
        ),
        child: ListView(
          padding: EdgeInsets.all(16.0),
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 5.0),
              child: Row(
                children: _indicadoresImagen,
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                      '\$ ${widget.ropaModel.precio}',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        color: Color(0xff283037),
                        fontSize: 30.0,
                        fontFamily: 'Montserrat',
                      )
                  ),
                ),
                RaisedButton(
                  onPressed: (){},
                  color: Colors.redAccent,
                  child: Text(
                      'Comprar',
                      style: TextStyle(
                        fontSize: 20.0,
                        fontFamily: 'Montserrat',
                        color: Colors.white,
                      )
                  ),
                  shape: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.circular(20.0)
                  ),
                )
              ],
            ),
            Text(
                listaTipoRopaModelo.firstWhere((tipo) => tipo['id'] == widget.ropaModel.idTipo )['descripcion'],
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 25.0,
                  fontFamily: 'Montserrat',
                  color: Color(0xff283037),
                )
            ),
            Text(
                listaTienda.firstWhere((tienda) => tienda.id == widget.ropaModel.idTienda ).nombre,
                style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Montserrat',
                  color: Color(0xff283037),
                )
            ),
            SizedBox(height: 10.0,),
            Text(
                widget.ropaModel.descripcion,
                style: TextStyle(
                  fontSize: 28.0,
                  fontFamily: 'Montserrat',
                  color: Color(0xff283037),
                )
            ),
          ],
        ),
      ),
    );
  }
}
