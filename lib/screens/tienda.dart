import 'package:flutter/material.dart';
import 'package:seline_app/models/TiendaModel.dart';
import 'package:seline_app/models/RopaModel.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:seline_app/screens/crearTienda.dart';
import 'package:seline_app/screens/detalleRopa.dart';

class Tienda extends StatefulWidget {

  TiendaModel tiendaModel;

  Tienda({this.tiendaModel});

  @override
  _TiendaState createState() => _TiendaState();
}

class _TiendaState extends State<Tienda> {

  double _height;
  double _width;
  List _listaRopa;
  int _indiceTipoRopa = 0;

  @override
  void initState() {
    super.initState();
    if(widget.tiendaModel != null){
      getListaRopa();
    }
  }

  void getListaRopa(){
    setState(() {
      if(_indiceTipoRopa == 0){
        _listaRopa = listaRopaModelo.where((ropa)=> ropa.idTienda == widget.tiendaModel.id).toList();
      } else {
        _listaRopa = listaRopaModelo.where((ropa)=>ropa.idTipo == _indiceTipoRopa).toList();
      }
    });
  }

  void _onTapRopa(ropa){
    Navigator.of(context).push(MaterialPageRoute(builder: (context)=>DetalleRopa(ropaModel: ropa,)));
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: widget.tiendaModel != null ? CustomScrollView(
        slivers: <Widget>[
          sliverAppBar(),
          listaRopaView()
        ],
      ) : screenCrearTienda(),
    );
  }

  Widget screenCrearTienda(){
    return Center(
      child: Container(
        child: GestureDetector(
//          onTap: ()=>print('hola'),
          child: CrearTienda(),
        ),
      ),
    );
  }

  Widget sliverAppBar(){
    return SliverAppBar(
      flexibleSpace: cabeceraTienda(),
      expandedHeight: _height / 3.5,
      backgroundColor: Colors.white,
      title: Text(
        widget.tiendaModel.nombre,
      ),
    );
  }

  Widget cabeceraTienda(){
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Image.network(widget.tiendaModel.urlImagen,fit: BoxFit.fill,),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: _height / 6,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.transparent, Colors.white],
                  end: Alignment.bottomCenter,
                  begin: Alignment.topCenter,
                )
            ),
          ),
        )
      ],
    );
  }

  Widget listaRopaView(){
    return SliverPadding(
      padding: EdgeInsets.all(8.0),
      sliver: SliverStaggeredGrid.countBuilder(
        staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
        crossAxisCount: 2,
        itemCount: _listaRopa.length,
        itemBuilder:
            (context,index){
          return InkWell(
            onTap: ()=> _onTapRopa(_listaRopa[index]),
            child: Card(
              elevation: 5.0,
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                      borderRadius: BorderRadius.circular(3.0),
//                      child: Hero(
//                        tag: 'tag-first-image-${_listaRopa[index].id}',
                      child: Image.network(_listaRopa[index].urlImages[0],fit: BoxFit.fill,width: double.infinity,)
//                      ),
                  ),
                  Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(3.0),bottomRight: Radius.circular(3.0)),
                          gradient: LinearGradient(
                              colors: [Colors.transparent,Colors.black54],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter
                          )
                      ),
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                              _listaRopa[index].descripcion,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 20.0,
                                  fontFamily: 'Montserrat',
                                  color: Colors.white
                              )
                          ),
                          Text(
                              '\$ ${_listaRopa[index].precio.toString()}',
                              style: TextStyle(
                                  fontSize: 22.0,
                                  fontFamily: 'Montserrat',
                                  color: Colors.white
                              )
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
